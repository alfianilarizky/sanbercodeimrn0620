//import Component from './components/component'
import React, {Component} from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoitem'
import data from './data.json'

export default class App extends Component {
    

    render() {
        
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('./images/logo.png')}
                    style = {styles.image} />
                    <View style={styles.rightNav}>
                        <TouchableOpacity><Icon style={styles.navItem} name='search' size={25}/>
                        </TouchableOpacity>
                        <TouchableOpacity><Icon style={styles.navItem} name='account-circle' size={25}/>
                        </TouchableOpacity>
                        
                    </View>
                </View>
                <View style={styles.body}>
                <FlatList
                    data={data.items}
                    renderItem={(video)=><VideoItem video={video.item} />}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                />
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity style ={styles.tabItem}>
                        <Icon name='home' size={25}/>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style ={styles.tabItem}>
                        <Icon name='whatshot' size={25} />
                        <Text style={styles.tabTitle}>Trending</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style ={styles.tabItem}>
                        <Icon name='subscriptions' size={25} />
                        <Text style={styles.tabTitle}>Subscriptions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style ={styles.tabItem}>
                        <Icon name='folder' size={25} />
                        <Text style={styles.tabTitle}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container : {
        flex: 1,
    },

    navBar : {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },

    rightNav: {
        flexDirection : 'row',

    },
    navItem: {
        marginLeft : 25,
    },
    tabBar : {
        backgroundColor : 'white',
        height: 60,
        borderTopWidth : 0.5,
        borderColor: '#E5E5E5',
        flexDirection : 'row',
        justifyContent : 'space-around' 
    },
    body : {
        flex:1,
    },
    tabItem:{
        alignItems: 'center',
        justifyContent: 'center',

    },
    tabTitle: {
        fontSize : 11,
        color: '#3C3C3C',
    },

    box: {
        width: 200,
        height: 200,
        backgroundColor: 'skyblue',
        borderWidth: 2,
        borderColor: 'steelblue',
        borderRadius: 20,
      },
    text: {
        backgroundColor: 'whitesmoke',
        color: '#4A90E2',
        fontSize: 18,
        padding: 10,
      },
    image: {
        width: 98,
        height: 22,
      },

      row: {
        padding: 15,
        marginBottom: 5,
        backgroundColor: 'skyblue',
      },
})