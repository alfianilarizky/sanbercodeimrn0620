import React from 'react';
import {
    View, Text, Image, StyleSheet, Platform, FlatList
} from 'react-native';
//import Constants from 'expo-constants';
//import data from './skillData.json';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';



function Item({ isi }) {
  return (
      <View style={styles.itemlistView}>
          <View style={styles.item1View}>
              <Icon name={isi.iconName} size={70} color="#003366" style={styles.icon} />
          </View>
          <View style={styles.item2View}>
              <Text style={styles.itemSkillText}>{isi.skillName}</Text>
              <Text style={styles.itemCatText}>{isi.categoryName}</Text>
              <Text style={styles.itemPersenText}>{isi.percentageProgress}</Text>
          </View>
          <View style={styles.item3View}>
              <IonIcon name="ios-arrow-forward" size={70} color="#003366" style={styles.icon} />
          </View>
      </View>
  );
}

export default class App extends React.Component {
    state = {
        data: []
    };
  
    // Mount User Method
    componentDidMount() {
        this.fetchData();
      }

    fetchData = async() => {
        const response = await fetch('https://api.kawalcorona.com/indonesia/');
        const json = await response.json();
        this.setState({data : json});
    }

    render() {
        return (
            <View style={{flex: 1, paddingTop: 20, flexDirection:'column'}}>
              <Text style={{fontSize: 18, fontWeight: 'bold', alignSelf:'center'  }}>Data Covid-19 Indonesia</Text>
              <View style={{borderBottomColor: 'black', borderBottomWidth: 1, marginLeft:10, marginRight:10}}/>
              <FlatList
                data={this.state.data}
                keyExtractor={(x,i) => i}
                renderItem={({ item }) =>
                    <View style={{flexDirection:'column', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{flexDirection:'row'}}>
                          <View style={styles.tabkoronapositif}> 
                            <Text style={{marginLeft:10, fontWeight:'bold', alignSelf:'center', marginTop:5}}>{`Positif ${item.positif}`}</Text> 
                          </View>
                          <View style={styles.tabkoronasembuh}> 
                            <Text style={{marginLeft:10, fontWeight:'bold', alignSelf:'center', marginTop:5}}>{`Sembuh ${item.sembuh}`}</Text> 
                          </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                          <View style={styles.tabkoronameninggal}> 
                            <Text style={{marginLeft:10, fontWeight:'bold', alignSelf:'center', marginTop:5}}>{`Meninggal ${item.meninggal}`}</Text> 
                          </View>
                          <View style={styles.tabkoronadirawat}> 
                            <Text style={{marginLeft:10, fontWeight:'bold', alignSelf:'center', marginTop:5}}>{`Dirawat ${item.dirawat}`}</Text> 
                          </View>
                        </View>
                        <View style={{alignItems:'stretch', marginLeft:10, marginRight:10, borderWidth:1, backgroundColor:'#CD5C5C', marginTop:20, height:250, borderRadius:10}}>
                          <Text style={{marginLeft:10, marginRight:10, marginTop:10}}>Data diambil dari API https://api.kawalcorona.com/indonesia/</Text>
                        </View>
                        
                    </View>
                    
                }/>
            </View>
          );
        }

    }

  
  const styles = StyleSheet.create({
    viewList: {
      height: 100,
      flexDirection: 'row',
      borderWidth: 1,
      borderColor: '#DDD',
      alignItems: 'center'
    },
    Image: {
      width: 88,
      height: 80,
      borderRadius: 40
    },
    textItemLogin: {
      fontWeight: 'bold',
      textTransform: 'capitalize',
      marginLeft: 20,
      fontSize: 16
    },
    textItemUrl: {
      fontWeight: 'bold',
      marginLeft: 20,
      fontSize: 12,
      marginTop: 10,
      color: 'blue'
    },
    tabkoronasembuh:{
      borderWidth:1, 
      marginTop: 20, 
      marginLeft:10, 
      marginRight:10, 
      backgroundColor:'#3EC6FF',
      height:30,
      alignContent:'center',
      marginRight:10,
      width:150,
      borderRadius:10
    },
    tabkoronapositif:{
      borderWidth:1, 
      marginTop: 20, 
      marginLeft:10, 
      marginRight:10, 
      backgroundColor:'#DC143C',
      height:30,
      alignContent:'center',
      marginRight:10,
      width:150,
      borderRadius:10
    },
    tabkoronameninggal:{
      borderWidth:1, 
      marginTop: 20, 
      marginLeft:10, 
      marginRight:10, 
      backgroundColor:'#696969',
      height:30,
      alignContent:'center',
      marginRight:10,
      width:150,
      borderRadius:10
    },
    tabkoronadirawat:{
      borderWidth:1, 
      marginTop: 20, 
      marginLeft:10, 
      marginRight:10, 
      backgroundColor:'#ADFF2F',
      height:30,
      alignContent:'center',
      marginRight:10,
      width:150,
      borderRadius:10
    }
  })