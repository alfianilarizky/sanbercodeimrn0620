import React from "react";

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import LoginScr from './LoginScreen';
import AboutScr from './AboutScreen';
import Homing from './Home';
import ProjectScr from './PrejectScreen';

const Stack = createStackNavigator();
const TabsStack = createBottomTabNavigator();

const StackScreen = () => (
    <Stack.Navigator>
        <Stack.Screen name='LoginScreen' component={LoginScr}
            options={{
                title : 'Login'
            }}
        />
        <Stack.Screen name='TabStackScreen' component={TabStackScreen}
            options={{
                title: 'Aplikasiku'
            }}
        />
    </Stack.Navigator>
);

const TabStackScreen = () => (
    <TabsStack.Navigator >
        <TabsStack.Screen name='Homing' component={Homing}
            options={{
                title: 'Home'
            }} />
        <TabsStack.Screen name='ProjectScreen' component={ProjectScr}
            options={{
                title: 'My Project'
            }}
        />
        <TabsStack.Screen name='AboutScr' component={AboutScr}
            options={{
                title: 'About Me'
            }}
        />
    </TabsStack.Navigator>
);

export default () => (
    <NavigationContainer>
        <StackScreen />
    </NavigationContainer>
);